function luckySevens() {

	var bankrollStartValue = (document.getElementById("bankrollField").value);
	var bankrollValue = Number(bankrollStartValue);	

	var dice1 = 0;
	var dice2 = 0;
	var diceTotal = 0;

	var profitPerWin = 4;
	var lossPerLoss = 1;

	var maxWon = bankrollValue;
	var rollsAtMaxWon = 0;
	var currentRolls = 0;
	var numWins = 0;
	var resultsDistribution = [0,0,0,0,0,0,0,0,0,0,0,0,0];

	var resultsPerRollBuffer = "";
	var statisticsBuffer = "";	

	var resultsBody = document.getElementById("resultsBody");
	var statisticsBody = document.getElementById("statisticsBody");
	var resultsPerRollBody = document.getElementById("resultsPerRollBody");	

	var i = 0;


	if (bankrollValue < lossPerLoss) {
		
		document.getElementById("bankrollAlert").innerHTML = "You must have at least $1 in your bankroll!";	
		return alert("You must have at least $1 in your bankroll!");

	} else if (isNaN(bankrollStartValue)) {
    
    	document.getElementById("bankrollAlert").innerHTML = "Please enter your bet as a number, with no symbols.";	
    	return alert("Please enter your bet as a number, with no symbols.");

	} else {

		clearResults();
		createResultsPerRollHeader();
		playAndCreateResults();
		displayResults();		
		createAndDisplayStatistics();
		createResultsPerRollFooter();
		changePlayButtonToPlayAgainButton();

	}

	function clearResults() {

		document.getElementById("bankrollAlert").innerHTML = "";	
		document.getElementById("resultsPerRollBody").innerHTML = "";
		document.getElementById("resultsPerRollBody").style.visibility = "hidden";	
		document.getElementById("statisticsBody").innerHTML = "";
		document.getElementById("statisticsBody").style.visibility = "hidden";		
	}

	function createResultsPerRollHeader() {

		if (document.getElementById("resultsPerRollCheckbox").checked) {
	
			resultsPerRollBuffer = "<table class=\"table-responsive resultsPerRollTable\"><caption class=\"resultsCaption\"><span>Per-Roll Results</span></caption><thead><tr><th class=\"resultsHeaderFieldName\">Roll #</th><th class=\"resultsHeaderFieldName\">Dice Result</th><th class=\"resultsHeaderFieldName\">Current $</th><th class=\"resultsHeaderFieldName\">Most $ Held</th><th class=\"resultsHeaderFieldName\">Roll # @ Most Held</th></tr></thead><tbody>";
			
		}	

	}

	function playAndCreateResults() {
				
		while (bankrollValue >= lossPerLoss) {

			dice1 = Math.floor(Math.random() * 6) + 1;
			dice2 = Math.floor(Math.random() * 6) + 1;
			diceTotal = dice1 + dice2;

			updateBankrollAndStats();
			updateResultsPerRoll();
			createAndDisplayStatistics();		
	
		}

	}

	function updateBankrollAndStats() {

		currentRolls++;

		if (diceTotal == 7) {

			bankrollValue += profitPerWin;
			numWins++;

		} else {

			bankrollValue -= lossPerLoss;

		}

		if (bankrollValue > maxWon) {

			maxWon = bankrollValue;
			rollsAtMaxWon = currentRolls;

		}		

		resultsDistribution[diceTotal]++;

	}

	function updateResultsPerRoll() {

		if (document.getElementById("resultsPerRollCheckbox").checked) {

			resultsPerRollBuffer += "<tr><td class=\"resultsData\">" + currentRolls + "</td><td class=\"resultsData\">" + diceTotal + "</td><td class=\"resultsData\">" + "$" + bankrollValue.toFixed(2) + "</td><td class=\"resultsData\">" + "$" + (Number(maxWon)).toFixed(2) + "</td><td class=\"resultsData\">" + rollsAtMaxWon + "</td></tr>";

		}				

	}
				
	function displayResults() {

		document.getElementById("startingBetHeaderField").innerHTML = "$" + (Number(bankrollStartValue)).toFixed(2);	
		document.getElementById("totalRollsField").innerHTML = currentRolls;
		document.getElementById("maxWonField").innerHTML = "$" + (Number(maxWon)).toFixed(2);
		document.getElementById("rollsAtMaxWonField").innerHTML = rollsAtMaxWon;
		
		resultsBody.style.visibility = "visible";	

	}

	function createAndDisplayStatistics() {

			if (document.getElementById("statisticsCheckbox").checked) {

				statisticsBuffer = "<table class=\"table-responsive statisticsTable\"><caption class=\"resultsCaption\"><span>Statistics</span></caption><tbody>" + "<tr><td class=\"resultsDataFieldNames\"># Rolls</td><td class=\"resultsData\">" + currentRolls + "</td></tr>" + "<td class=\"resultsDataFieldNames\"># Wins</td><td class=\"resultsData\">" + numWins + "</td></tr>" + "<td class=\"resultsDataFieldNames\">% Wins</td><td class=\"resultsData\">" + ((numWins / currentRolls) * 100).toFixed(2) + "%</td></tr>" + "<td class=\"resultsDataFieldNames\">Expected # Wins</td><td class=\"resultsData\">" + (currentRolls*(0.1667)).toFixed(2) + "</td></tr>" + "<td class=\"resultsDataFieldNames\">Expected % Wins*</td><td class=\"resultsData\">16.67%</td></tr>";

				for (i=2; i<resultsDistribution.length;i++) {
						
						statisticsBuffer += "<tr><td class=\"resultsDataFieldNames\"># of " + i + "s / % of " + i + "s</td><td class=\"resultsData\">" + resultsDistribution[i] + " / " + ((resultsDistribution[i] / currentRolls) * 100).toFixed(3) + "%</td></tr>";
					}
					
				statisticsBuffer += "</tbody></table><span class=\"smallPrint\">*From <a href=\"http://wizardofodds.com/gambling/dice/\">Wizard of Odds</a></span>";

				document.getElementById("statisticsBody").innerHTML = statisticsBuffer;
				
				statisticsBody.style.visibility = "visible";

			}

	}

	function createResultsPerRollFooter() {

		if (document.getElementById("resultsPerRollCheckbox").checked) {

			resultsPerRollBuffer += "</tbody></table>";
			document.getElementById("resultsPerRollBody").innerHTML = resultsPerRollBuffer;

			resultsPerRollBody.style.visibility = "visible";

		}

	}

	function changePlayButtonToPlayAgainButton() {

		document.getElementById("playButton").innerHTML = "Play Again?";

	}

}