function checkForm() {

	var formAlertBuffer = "";
	var name = document.getElementById("name").value;
	var email = document.getElementById("email").value;
	var phone = document.getElementById("phone").value;
	var reason = document.getElementById("reason").value;
	var comments = document.getElementById("comments").value;
	var daysList = document.querySelectorAll("input[value=MondayCheckbox], input[value=TuesdayCheckbox], input[value=WednesdayCheckbox], input[value=ThursdayCheckbox], input[value=FridayCheckbox]");
	var someDayChecked = false;
	var formError = false;
	
	if (trim(name) == "") {
	
		formAlertBuffer += "Please enter your name.\n";
		document.getElementById("nameAlert").innerHTML = "Please enter your name.";
		formError = true;

	}

	if ((trim(email) == "") && (trim(phone) == "")) {

		formAlertBuffer += "Please enter your email address or phone number.\n";
		document.getElementById("emailAlert").innerHTML = "Please enter your email address or phone number.";	
		document.getElementById("phoneAlert").innerHTML = "Please enter your email address or phone number.";			
		formError = true;

	}


	if ((reason == "other") && (trim(comments) == "")) {

		formAlertBuffer += "You have selected 'Other' under 'Reason for Inquiry'. Please enter additional information in the box provided.\n";
		document.getElementById("commentsAlert").innerHTML = "You have selected 'Other' under 'Reason for Inquiry'.<br/>Please enter additional information in the box provided.";		
		formError = true;
		
	}

	for (i = 0; i <	daysList.length; i++) {
   		
   		if (daysList[i].checked == true) {
   			someDayChecked = true;
   			break;
   		}

	}

	if (!someDayChecked) {

		formAlertBuffer += "Please check the best day to contact you.\n";
		document.getElementById("daysAlert").innerHTML = "Please check the best day to contact you.";			
		formError = true;

	}
	
	function trim (string) {
	
		return string.replace (/['!"#$%&\\'()\*+,\-\.\/:;<=>?@\[\\\]\^_`{|}~']/g,"");	
	
	}

	
	if (formAlertBuffer) {
	
		alert(formAlertBuffer);
	
	}

	if (formError) {
		return false;
	}
}